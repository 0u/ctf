export EDITOR=nvim

alias v=\$EDITOR vim=\$EDITOR
alias \
	g="git" gp="git push" gc="git commit" gs="git status" \
	gu="git pull" gd="git diff" ga="git add" gg="git clone"


