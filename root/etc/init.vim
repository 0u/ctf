set ts=2 sw=2 et
syntax enable
filetype plugin indent on

set encoding=utf-8
set ruler showmode noshowcmd
set autoread                 " read changes outside of vim automatically
set noerrorbells             " begone beeps
set autowrite                " Automatically save before :next, :make etc.
set hidden                   " Non retarded buffers
set history=1000             " vim ex mode history
set fileformats=unix,dos,mac " Prefer Unix over Windows over OS 9 formats
set copyindent               " copy existing indentation
set nohlsearch               " Don't highlight all search matches.
set linebreak
set number relativenumber
set numberwidth=1            " Use the least amount of space possible
set laststatus=0             " disable status bar

" Security
set nomodelineexpr nomodeline
set modelines=0
set fillchars="fold: ,stl: ,stlnc: "

" Better pattern matching
set ignorecase
set smartcase

" Splits open at the bottom and right
set splitbelow splitright

" Tweaks for file browsing
let g:netrw_banner=0     " disable anoying banner
let g:netrw_liststyle=3  " tree view
let g:netrw_winsize = 25 " window size

" Center the screen when searching
nn n nzz

" make Y non retarded
nn Y y$
