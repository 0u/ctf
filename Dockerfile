# Docker container for pentesting

FROM alpine:latest

# Copy files over
COPY ./root /

# Delete disabled default files in /etc/profile.d
RUN rm -f /etc/profile.d/color_prompt /etc/profile.d/locale

# Setup user
RUN adduser -D -H user
RUN chown -R user /home/user

# Install software
RUN apk add \
	build-base nmap nmap-ncat \
	python3 python3-dev git \
	neovim man man-pages \
	tmux wget curl exiftool

RUN pip3 install --upgrade pip
RUN pip3 install requests

ENTRYPOINT ["su", "-l", "root"]
